# CPPGDC 4.0
Reworked the whole CPP Game Devevelopment Club Website for the 2021-22 School Year

###### Home Page
Has a gallery with rotating pictures, Discord and registration links available straight away.

###### Contact Page
Updated with forms using FormSpree. Multiple methods of contact.

###### Updates Page
This page will display all the event titles and descriptions based on what type of event the club presented. A club calendar is present along with a newsletter too.

###### Future Features
- Mobile Optimization
- Merchandise Store
- Twitch/Youtube Video Access

###### Updated Officer Profile Pictures
Officer pictures were updated for years 21-22 and given appropriate roles + descriptions. 
